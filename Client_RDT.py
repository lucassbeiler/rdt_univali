import time
import socket
import hashlib
import struct
import base64
import random

RDT_SPEC = 30 # (Valores possíveis: 21, 22 e 30). É necessário alterar também no outro arquivo.

# Cliente (Remetente RDT)

SERVER_PORT = 6666
SERVER_ADDR = "127.0.0.1"

# Função que calcula a soma de verificação
def calculate_checksum(acknum, seqnum, payload):
    keys = (acknum, seqnum, payload)
    pkt_struct = struct.Struct('I I 48s').pack(*keys)
    checksum = hashlib.md5(pkt_struct).hexdigest().encode('utf-8')
    return checksum

## Função que contrói o pacote a ser enviado (constrói sndpkt do FSM).
def compose_pkt(last_acknum, payload, checksum):
    global last_seqnum

    # Simula adulteração aleatória do campo PAYLOAD.
    if random.random() < 0.5:
        payload = "dsfsdkf" # Simula payload adulterado.

    # Simula adulteração aleatória dos campos ACKNUM e SEQNUM.
    if random.random() < 0.5:
        keys = (123, 456,  base64.b64encode(payload.encode('utf-8')), checksum)
        pkt_struct = struct.Struct('I I 48s 32s').pack(*keys)
        return pkt_struct
    else: # Tão aleatoriamente quanto antes, envia os pacotes íntegros.
        keys = (last_acknum, last_seqnum,  base64.b64encode(payload.encode('utf-8')), checksum)
        pkt_struct = struct.Struct('I I 48s 32s').pack(*keys)
        return pkt_struct

## Função que envia pacote para o servidor.
def rdt_send(payload):
    global last_seqnum
    global last_acknum

    # Obtém soma de verificação
    keys = (last_acknum, last_seqnum, base64.b64encode(payload.encode('utf-8')))
    pkt_struct = struct.Struct('I I 48s').pack(*keys)
    checksum = hashlib.md5(pkt_struct).hexdigest().encode('utf-8')

    # Gera pacote, incluindo soma de verificação.
    sndpkt = compose_pkt(last_acknum, payload, checksum)

    # Efetua envio (ação udt_send do FSM) do pacote através de nosso socket UDP
    sckt.sendto(sndpkt, (SERVER_ADDR, SERVER_PORT))

## Verifica se o campo acknum possui o valor esperado.
def is_ack(received_packet, expected_acknum):
    global last_seqnum, last_acknum
    received_packet_acknum = received_packet[0]
    received_packet_seqnum = received_packet[1]
    if received_packet_acknum == expected_acknum and received_packet_seqnum == last_seqnum: # Verifica se é ACK e se seu SEQNUM é igual ao do último pacote emitido.
        return True
    else:
        return False

## Calcula a soma de verificação do pacote recebido e compara com a soma de verificação contida no campo checksum do pacote.
def is_corrupted(received_packet):
    current_checksum = calculate_checksum(received_packet[0], received_packet[1], received_packet[2])
    received_packet_checksum  = received_packet[3]

    if received_packet_checksum != current_checksum:
        return True
    else:
        return False

## Recebe as respostas do servidor e as interpreta. Stop and wait: Aguardando um ACK antes de prosseguir com a próxima sequência.
def rdt_receive(received_packet):
    global last_acknum
    global last_seqnum

    if not is_corrupted(received_packet) and is_ack(received_packet, last_acknum + 1): # ACK respondido e não corrompido: A próxima sequência será enviada.
        print("ACK recebido: O próximo pacote será enviado.\n")
        last_seqnum = (last_seqnum + 1) % 2 # Bump para a próxima sequência # VALORES POSSÍVEIS SÃO ZERO E UM.
        return True
    else: # Tudo que não for um ACK válido cairá neste else (NAKs, pacotes corrompidos e etc), levando-nos à retransmissão do pacote enviado anteriormente.
        if RDT_SPEC == 21:
            if received_packet[0] == 2:
                print(f"Pacote inválido: \n\tACKNUM: {received_packet[0]} (2 = NAK) \n\tSEQNUM: (Nulo. rdt2.1 não retorna SEQNUM no ACK/NAK) \n\tChecksum: {received_packet[3]}\n")
            else:
                print(f"Pacote inválido: \n\tACKNUM: {received_packet[0]} \n\tSEQNUM: (Nulo. rdt2.1 não retorna SEQNUM no ACK/NAK) \n\tChecksum: {received_packet[3]}\n")
        else:
            print(f"ACK inválido: \n\tACKNUM: {received_packet[0]} \n\tSEQNUM: {received_packet[1]} \n\tChecksum: {received_packet[3]}")
        print("Tentando novamente...\n")
        return False

last_seqnum = 0
last_acknum = 0
sckt = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # Inicializa socket UDP (SOCK_DGRAM) sobre IPv4 (AF_INET)

app_payloads = ["abcdefghijklmnopqrst", "12345678901234567890", "09876543210987654321" ]

# Tenta até que o pacote seja recebido pelo servidor e um ACK válido seja retornado.
for app_payload in app_payloads:
    while True:

        # Recebe e apresenta os dados.
        if(RDT_SPEC == 30):
            sckt.settimeout(2) # Temporizador de 2 segundos para rdt3.0 via SOCKET TIMEOUT.
        try:
            # Envia dado da aplicação
            rdt_send(app_payload)
            print('Payload enviado: ', app_payload)
            packet, addr = sckt.recvfrom(3092)
            received_packet = struct.Struct('I I 48s 32s').unpack(packet)
            if rdt_receive(received_packet): 
                print(f"Resposta de {addr[0]}:{addr[1]}:\n\t ACKNUM: {received_packet[0]} (1 = ACK)\n\t SEQNUM: {received_packet[1]}\n\t Payload: (ACK não retorna payload)\n\t Checksum: {received_packet[3].decode('ascii')}")
                break # Servidor retornou um ACK e este foi entregue sem erros.
        except socket.timeout:
            print('Temporizador ultrapassado! Retransmitindo pacote...\n\n')
            continue
        

print('Comunicação finalizada satisfatoriamente!\n')