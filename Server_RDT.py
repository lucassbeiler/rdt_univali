import struct
import socket
import hashlib
import base64
import random

RDT_SPEC = 30 # (Valores possíveis: 21, 22 e 30). É necessário alterar também no outro arquivo.

# Servidor (Destinatário RDT)

## Função que contrói o pacote a ser enviado.
def compose_pkt(last_acknum, last_seqnum, data, checksum):
    keys = (last_acknum, last_seqnum, data, checksum)
    pkt_struct = struct.Struct('I I 48s 32s').pack(*keys)
    return pkt_struct

# Função que calcula a soma de verificação
def calculate_checksum(acknum, seqnum, payload):
    keys = (acknum, seqnum, payload)
    pkt_struct = struct.Struct('I I 48s').pack(*keys)
    checksum = hashlib.md5(pkt_struct).hexdigest().encode('utf-8')
    return checksum

## Calcula a soma de verificação do pacote recebido e compara com a soma de verificação contida no campo checksum do pacote.
def is_corrupted(rcvpkt):
    current_checksum = calculate_checksum(rcvpkt[0], rcvpkt[1], rcvpkt[2])
    rcvpkt_checksum  = rcvpkt[3]

    if rcvpkt_checksum != current_checksum:
        return True
    else:
        return False

# Inicializa socket UDP e define endereçamento (incluindo porta)
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP sobre IPv4
sock.bind(("0.0.0.0", 6666))
print("Servidor em execução...")

while True:
    recv, addr = sock.recvfrom(3092) 
    recvpkt = struct.Struct('I I 48s 32s').unpack(recv)
    print(f"Chamada de {addr[0]}:{addr[1]} (cliente):\n\t ACK: {recvpkt[0]}\n\t SEQ: {recvpkt[1]}\n\t Payload: {base64.b64decode(recvpkt[2]).decode('ascii')}\n\t Checksum: {recvpkt[3].decode('ascii')}")

    if RDT_SPEC == 21:
        if not is_corrupted(recvpkt):
            print('Pacote íntegro!')

            # Prepara pacote ACK e sua checksum.
            checksum = calculate_checksum(recvpkt[0] + 1, recvpkt[1], b'')

            sndpkt = compose_pkt(recvpkt[0] + 1, recvpkt[1], b'', checksum)
            print('Preparando ACK...')

            # Envia ACK
            sock.sendto(sndpkt, addr)
            print('ACK Enviado!\n')
        else: # Retorna o ACK do último pacote sucedido. Saiba que seqnum é 0 ou 1 em protocolos stop and wait.
            print('Pacote corrompido!\n')

            # Ordem dos parâmetros abaixo: ACKNUM, SEQNUM, PAYLOAD, CHECKSUM.
            checksum = calculate_checksum(2, 0, b'')
            sndpkt = compose_pkt(2, 0, b'', checksum) 
            sock.sendto(sndpkt, addr)
    
    
    if RDT_SPEC == 22 or RDT_SPEC == 30:
        if not is_corrupted(recvpkt):
            print('Pacote íntegro!')

            # Prepara pacote ACK e sua checksum.
            checksum = calculate_checksum(recvpkt[0] + 1, recvpkt[1], b'')

            sndpkt = compose_pkt(recvpkt[0] + 1, recvpkt[1], b'', checksum)
            print('Preparando ACK...')

            # Envia ACK
            if(RDT_SPEC == 30 and (random.random() < 0.5)):
                print("Desviando propositalmente ACK...")
            else:
                sock.sendto(sndpkt, addr)
                print('ACK Enviado!\n')
        else: # Retorna o ACK do último pacote sucedido. Saiba que seqnum é 0 ou 1 em protocolos stop and wait.
            print('Pacote corrompido!\n')

            # Ordem dos parâmetros abaixo: ACKNUM, SEQNUM, PAYLOAD, CHECKSUM.
            checksum = calculate_checksum(recvpkt[0] + 1, (recvpkt[1] + 1) % 2, b'') # VALORES POSSÍVEIS PARA ACKNUM E SEQNUM SÃO ZERO E UM.
            sndpkt = compose_pkt(recvpkt[0] + 1, (recvpkt[1] + 1) % 2, b'', checksum)  # VALORES POSSÍVEIS PARA ACKNUM E SEQNUM SÃO ZERO E UM.
            sock.sendto(sndpkt, addr)
